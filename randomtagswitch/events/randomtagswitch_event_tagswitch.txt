namespace = tag_switch_event

# tag switch
country_event =
{
	id = tag_switch_event.2
	title = "tag_switch_event.EVTNAME2"
	desc = "tag_switch_event.EVTDESC2"
	picture = OVEREXTENSION_eventPicture
	
	is_triggered_only = yes
	
	option =
	{
		name = "tag_switch_event.EVTOPTA2"
		
		random_country =
		{
			limit =
			{
				ai = yes
				
				NOT
				{
					tag = REB
					tag = PIR
					tag = NAT
				}
				
				OR =
				{
					PIR =
					{
						NOT =
						{
							is_variable_equal =
							{
								which = tag_switch_variable_mode
								value = 1
							}
						}
					}
					
					AND =
					{
						NOT =
						{
							government = republic
						}
						
						OR =
						{
							is_variable_equal =
							{
								which = tag_switch_variable_monarch_death_regency
								value = 1
							}
							
							has_regency = no
						}
					}
				}
				
				OR =
				{
					PIR =
					{
						is_variable_equal =
						{
							which = tag_switch_variable_primitives
							value = 0
						}
					}
					
					primitives = no
				}
			}
			
			PREV =
			{
				switch_tag = PREV
			}
		}
	}
	
	option =
	{
		name = "tag_switch_event.EVTOPTB2"
		
		trigger =
		{
			PIR =
			{
				check_variable =
				{
					which = tag_switch_variable_options
					value = 2
				}
			}
		}
		
		hidden_effect =
		{
			random_country =
			{
				limit =
				{
					ai = yes
				}
			}
		}
		
		random_country =
		{
			limit =
			{
				ai = yes
				
				NOT
				{
					tag = REB
					tag = PIR
					tag = NAT
				}
				
				OR =
				{
					PIR =
					{
						NOT =
						{
							is_variable_equal =
							{
								which = tag_switch_variable_mode
								value = 1
							}
						}
					}
					
					AND =
					{
						NOT =
						{
							government = republic
						}
						
						OR =
						{
							is_variable_equal =
							{
								which = tag_switch_variable_monarch_death_regency
								value = 1
							}
							
							has_regency = no
						}
					}
				}
				
				OR =
				{
					PIR =
					{
						is_variable_equal =
						{
							which = tag_switch_variable_primitives
							value = 0
						}
					}
					
					primitives = no
				}
			}
			
			PREV =
			{
				switch_tag = PREV
			}
		}
	}
	
	option =
	{
		name = "tag_switch_event.EVTOPTC2"
		
		trigger =
		{
			PIR =
			{
				check_variable =
				{
					which = tag_switch_variable_options
					value = 3
				}
			}
		}
		
		hidden_effect =
		{
			random_country =
			{
				limit =
				{
					ai = yes
				}
			}
			
			random_country =
			{
				limit =
				{
					ai = yes
				}
			}
		}
		
		random_country =
		{
			limit =
			{
				ai = yes
				
				NOT
				{
					tag = REB
					tag = PIR
					tag = NAT
				}
				
				OR =
				{
					PIR =
					{
						NOT =
						{
							is_variable_equal =
							{
								which = tag_switch_variable_mode
								value = 1
							}
						}
					}
					
					AND =
					{
						NOT =
						{
							government = republic
						}
						
						OR =
						{
							is_variable_equal =
							{
								which = tag_switch_variable_monarch_death_regency
								value = 1
							}
							
							has_regency = no
						}
					}
				}
				
				OR =
				{
					PIR =
					{
						is_variable_equal =
						{
							which = tag_switch_variable_primitives
							value = 0
						}
					}
					
					primitives = no
				}
			}
			
			PREV =
			{
				switch_tag = PREV
			}
		}
	}
	
	option =
	{
		name = "tag_switch_event.EVTOPTD2"
		
		trigger =
		{
			PIR =
			{
				is_variable_equal =
				{
					which = tag_switch_variable_option_stay
					value = 1
				}
			}
		}
	}
}
