namespace = tag_switch_event

# show options
country_event =
{
	id = tag_switch_event.3
	title = "tag_switch_event.EVTNAME3"
	desc = "tag_switch_event.EVTDESC3"
	picture = OVEREXTENSION_eventPicture
	
	is_triggered_only = yes
	
	# duration
	option =
	{
		name = "tag_switch_event.EVTOPTA3"
		
		country_event =
		{
			id = tag_switch_event.4
		}
	}
	
	# mode
	option =
	{
		name = "tag_switch_event.EVTOPTB3"
		
		country_event =
		{
			id = tag_switch_event.7
		}
	}
	
	# filter
	option =
	{
		name = "tag_switch_event.EVTOPTC3"
		
		country_event =
		{
			id = tag_switch_event.8
		}
	}
	
	# available options
	option =
	{
		name = "tag_switch_event.EVTOPTD3"
		
		country_event =
		{
			id = tag_switch_event.9
		}
	}
	
	# tag switch now
	option =
	{
		name = "tag_switch_event.EVTOPTE3"
		
		country_event =
		{
			id = tag_switch_event.2
		}
	}
	
	# close
	option =
	{
		name = "tag_switch_event.EVTOPTF3"
	}
}

# duration
country_event =
{
	id = tag_switch_event.4
	title = "tag_switch_event.EVTNAME4"
	desc = "tag_switch_event.EVTDESC4"
	picture = OVEREXTENSION_eventPicture
	
	is_triggered_only = yes
	
	# short
	option =
	{
		name = "tag_switch_event.EVTOPTA4"
		
		country_event =
		{
			id = tag_switch_event.5
		}
	}
	
	# long
	option =
	{
		name = "tag_switch_event.EVTOPTB4"
		
		country_event =
		{
			id = tag_switch_event.6
		}
	}
	
	# back
	option =
	{
		name = "tag_switch_event.EVTOPTRT"
		
		country_event =
		{
			id = tag_switch_event.3
		}
	}
}

#short duration
country_event
{
	id = tag_switch_event.5
	title = "tag_switch_event.EVTNAME5"
	desc = "tag_switch_event.EVTDESC5"
	picture = OVEREXTENSION_eventPicture
	
	is_triggered_only = yes
	
	option =
	{
		name = "tag_switch_event.EVTOPTA5"
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_duration
				value = 5
			}
		}
	}
	
	option =
	{
		name = "tag_switch_event.EVTOPTB5"
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_duration
				value = 10
			}
		}
	}
	
	option =
	{
		name = "tag_switch_event.EVTOPTC5"
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_duration
				value = 20
			}
		}
	}
	
	option =
	{
		name = "tag_switch_event.EVTOPTD5"
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_duration
				value = 25
			}
		}
	}
	
	option =
	{
		name = "tag_switch_event.EVTOPTRT"
		
		country_event =
		{
			id = tag_switch_event.4
		}
	}
}

# long duration
country_event
{
	id = tag_switch_event.6
	title = "tag_switch_event.EVTNAME6"
	desc = "tag_switch_event.EVTDESC6"
	picture = OVEREXTENSION_eventPicture
	
	is_triggered_only = yes
	
	option =
	{
		name = "tag_switch_event.EVTOPTA6"
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_duration
				value = 50
			}
		}
	}
	
	option =
	{
		name = "tag_switch_event.EVTOPTB6"
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_duration
				value = 100
			}
		}
	}
	
	option =
	{
		name = "tag_switch_event.EVTOPTC6"
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_duration
				value = 200
			}
		}
	}
	
	option =
	{
		name = "tag_switch_event.EVTOPTD6"
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_duration
				value = 250
			}
		}
	}
	
	option =
	{
		name = "tag_switch_event.EVTOPTRT"
		
		country_event =
		{
			id = tag_switch_event.4
		}
	}
}

# mode
country_event =
{
	id = tag_switch_event.7
	title = "tag_switch_event.EVTNAME7"
	desc = "tag_switch_event.EVTDESC7"
	picture = OVEREXTENSION_eventPicture
	
	is_triggered_only = yes
	
	# tag switch after time
	option =
	{
		name = "tag_switch_event.EVTOPTA7"
		
		trigger =
		{
			PIR =
			{
				NOT =
				{
					is_variable_equal =
					{
						which = tag_switch_variable_mode
						value = 0
					}
				}
			}
		}
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_mode
				value = 0
			}
		}
	}
	
	# tag switch on monarch death
	option =
	{
		name = "tag_switch_event.EVTOPTB7"
		
		trigger =
		{
			PIR =
			{
				NOT =
				{
					is_variable_equal =
					{
						which = tag_switch_variable_mode
						value = 1
					}
				}
			}
		}
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_mode
				value = 1
			}
		}
	}
	
	# manually tag switching
	option =
	{
		name = "tag_switch_event.EVTOPTC7"
		
		trigger =
		{
			PIR =
			{
				NOT =
				{
					is_variable_equal =
					{
						which = tag_switch_variable_mode
						value = 2
					}
				}
			}
		}
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_mode
				value = 2
			}
		}
	}
	
	# back
	option =
	{
		name = "tag_switch_event.EVTOPTRT"
		
		country_event =
		{
			id = tag_switch_event.3
		}
	}
}

# filter
country_event =
{
	id = tag_switch_event.8
	title = "tag_switch_event.EVTNAME8"
	desc = "tag_switch_event.EVTDESC8"
	picture = OVEREXTENSION_eventPicture
	
	is_triggered_only = yes
	
	# enable monarchy
	option =
	{
		name = "tag_switch_event.EVTOPTA8"
		
		trigger =
		{
			always = no
			
			PIR =
			{
				is_variable_equal =
				{
					which = tag_switch_variable_monarchy
					value = 0
				}
			}
		}
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_monarchy
				value = 1
			}
		}
	}
	
	# disable monarchy
	option =
	{
		name = "tag_switch_event.EVTOPTB8"
		
		trigger =
		{
			always = no
			
			PIR =
			{
				is_variable_equal =
				{
					which = tag_switch_variable_monarchy
					value = 1
				}
			}
		}
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_monarchy
				value = 0
			}
			
			set_variable =
			{
				which = tag_switch_variable_monarch_death_republics
				value = 1
			}
		}
	}
	
	# disable republics
	option =
	{
		name = "tag_switch_event.EVTOPTC8"
		
		trigger =
		{
			always = no
			
			PIR =
			{
				AND =
				{
					is_variable_equal =
					{
						which = tag_switch_variable_republic
						value = 0
					}
					
					OR =
					{
						is_variable_equal =
						{
							which = tag_switch_variable_monarch_death
							value = 0
						}
						
						is_variable_equal =
						{
							which = tag_switch_variable_monarch_death_republics
							value = 1
						}
					}
				}
			}
		}
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_republic
				value = 1
			}
		}
	}
	
	# enable republics
	option =
	{
		name = "tag_switch_event.EVTOPTD8"
		
		trigger =
		{
			always = no
			
			PIR =
			{
				AND =
				{
					is_variable_equal =
					{
						which = tag_switch_variable_republic
						value = 1
					}
					
					OR =
					{
						is_variable_equal =
						{
							which = tag_switch_variable_monarch_death
							value = 0
						}
						
						is_variable_equal =
						{
							which = tag_switch_variable_monarch_death_republics
							value = 0
						}
					}
				}
			}
		}
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_republic
				value = 0
			}
		}
	}
	
	# disable theocracies
	option =
	{
		name = "tag_switch_event.EVTOPTE8"
		
		trigger =
		{
			always = no
			
			PIR =
			{
				is_variable_equal =
				{
					which = tag_switch_variable_theocracy
					value = 0
				}
			}
		}
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_theocracy
				value = 1
			}
		}
	}
	
	# enable theocracies
	option =
	{
		name = "tag_switch_event.EVTOPTF8"
		
		trigger =
		{
			always = no
			
			PIR =
			{
				is_variable_equal =
				{
					which = tag_switch_variable_theocracy
					value = 1
				}
			}
		}
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_theocracy
				value = 0
			}
		}
	}
	
	# disable regencies
	option =
	{
		name = "tag_switch_event.EVTOPTG8"
		
		trigger =
		{
			PIR =
			{
				AND =
				{
					is_variable_equal =
					{
						which = tag_switch_variable_monarch_death
						value = 1
					}
					
					is_variable_equal =
					{
						which = tag_switch_variable_monarch_death_regency
						value = 0
					}
				}
			}
		}
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_monarch_death_regency
				value = 1
			}
		}
	}
	
	# enable regencies
	option =
	{
		name = "tag_switch_event.EVTOPTH8"
		
		trigger =
		{
			PIR =
			{
				AND =
				{
					is_variable_equal =
					{
						which = tag_switch_variable_monarch_death
						value = 1
					}
					
					is_variable_equal =
					{
						which = tag_switch_variable_monarch_death_regency
						value = 1
					}
				}
			}
		}
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_monarch_death_regency
				value = 0
			}
		}
	}
	
	# disable primitives
	option =
	{
		name = "tag_switch_event.EVTOPTI8"
		
		trigger =
		{
			PIR =
			{
				is_variable_equal =
				{
					which = tag_switch_variable_primitives
					value = 0
				}
			}
		}
		
		PIR = 
		{
			set_variable =
			{
				which = tag_switch_variable_primitives
				value = 1
			}
		}
	}
	
	# enable primitives
	option =
	{
		name = "tag_switch_event.EVTOPTJ8"
		
		trigger =
		{
			PIR =
			{
				is_variable_equal =
				{
					which = tag_switch_variable_primitives
					value = 1
				}
			}
		}
		
		PIR = 
		{
			set_variable =
			{
				which = tag_switch_variable_primitives
				value = 0
			}
		}
	}
	
	# back
	option =
	{
		name = "tag_switch_event.EVTOPTRT"
		
		country_event =
		{
			id = tag_switch_event.3
		}
	}
}

# available options
country_event =
{
	id = tag_switch_event.9
	title = "tag_switch_event.EVTNAME9"
	desc = "tag_switch_event.EVTDESC9"
	picture = OVEREXTENSION_eventPicture
	
	is_triggered_only = yes
	
	# 3 options
	option =
	{
		name = "tag_switch_event.EVTOPTA9"
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_options
				value = 3
			}
		}
	}
	
	# 2 options
	option =
	{
		name = "tag_switch_event.EVTOPTB9"
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_options
				value = 2
			}
		}
	}
	
	# 1 option
	option =
	{
		name = "tag_switch_event.EVTOPTC9"
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_options
				value = 1
			}
		}
	}
	
	# allow staying at current country
	option =
	{
		name = "tag_switch_event.EVTOPTD9"
		
		trigger =
		{
			PIR =
			{
				is_variable_equal =
				{
					which = tag_switch_variable_option_stay
					value = 0
				}
			}
		}
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_option_stay
				value = 1
			}
		}
	}
	
	# disallow staying at current country
	option =
	{
		name = "tag_switch_event.EVTOPTE9"
		
		trigger =
		{
			PIR =
			{
				is_variable_equal =
				{
					which = tag_switch_variable_option_stay
					value = 1
				}
			}
		}
		
		PIR =
		{
			set_variable =
			{
				which = tag_switch_variable_option_stay
				value = 0
			}
		}
	}
	
	# back
	option =
	{
		name = "tag_switch_event.EVTOPTRT"
		
		country_event =
		{
			id = tag_switch_event.3
		}
	}
}
