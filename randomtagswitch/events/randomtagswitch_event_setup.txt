namespace = tag_switch_event

# setup
country_event =
{
	id = tag_switch_event.1
	title = "tag_switch_event.EVTNAME1"
	desc = "tag_switch_event.EVTDESC1"
	picture = OVEREXTENSION_eventPicture
	
	is_triggered_only = yes
	fire_only_once = yes
	
	immediate =
	{
		hidden_effect =
		{
			PIR =
			{
				set_variable =
				{
					which = tag_switch_variable_mode
					value = 0
				}
				
				set_variable =
				{
					which = tag_switch_variable_duration
					value = 20
				}
				
				set_variable =
				{
					which = tag_switch_variable_options
					value = 3
				}
				
				set_variable =
				{
					which = tag_switch_variable_option_stay
					value = 1
				}
			}
		}
	}
	
	option =
	{
		name = "tag_switch_event.EVTOPTA1"
	}
	
	option =
	{
		name = "tag_switch_event.EVTOPTB1"
		
		country_event =
		{
			id = tag_switch_event.3
		}
	}
}
